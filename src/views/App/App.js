import './app.scss'

import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {withRouter} from 'react-router-dom'

import Header from 'components/Header'
import UserInfo from 'components/UserInfo'
import RepoList from 'components/RepoList'
import RepoInfo from 'components/RepoInfo'

import {getUser} from 'actions/userActions'
import {getRepos, setActiveRepo} from 'actions/reposActions'

import find from 'lodash.find'

const mapStateToProps = state => {
  return {
    user: state.user,
    repos: state.repos,
    activeRepo: state.activeRepo
  }
}

const mapDispatchToProps = dispatch => (bindActionCreators({
  getRepos,
  getUser,
  setActiveRepo
}, dispatch))

@withRouter
@connect(mapStateToProps, mapDispatchToProps)
export default class App extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    getRepos: PropTypes.func.isRequired,
    getUser: PropTypes.func.isRequired,
    setActiveRepo: PropTypes.func.isRequired,
    user: PropTypes.object,
    repos: PropTypes.array,
    activeRepo: PropTypes.object
  }

  componentDidMount () {
    const {getUser, getRepos} = this.props

    getUser()
    getRepos()
  }

  componentDidUpdate () {
    this._setActiveRepoFromRouter()
  }

  isLoading () {
    return !this.props.user.login || !this.props.repos.length
  }

  _setActiveRepoFromRouter () {
    const repoId = this.props.match.params.repoId
    const activeRepo = find(this.props.repos, (repo) => `${repo.id}` === repoId)

    if (activeRepo && activeRepo.id !== repoId) {
      this.props.setActiveRepo(activeRepo)
    }
  }

  render () {
    if (this.isLoading()) {
      return (<h1>Loading Please Wait</h1>)
    }

    return (
      <div styleName='app'>
        <Header />

        <div styleName='content'>
          <UserInfo user={this.props.user} />
          <RepoList
            repos={this.props.repos}
          />
          <RepoInfo repo={this.props.activeRepo} />
        </div>
      </div>
    )
  }
}

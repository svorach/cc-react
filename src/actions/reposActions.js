import {
  SET_REPOS,
  SET_ACTIVE_REPO
} from 'constants/actionTypes'

export function getRepos () {
  return (dispatch) => {
    setTimeout(() => {
      dispatch({
        type: SET_REPOS,
        payload: require('./repos.json')
      })
    }, 1000)
  }
}

export function setActiveRepo (repo) {
  return (dispatch) => {
    dispatch({
      type: SET_ACTIVE_REPO,
      payload: repo
    })
  }
}

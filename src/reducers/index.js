import {combineReducers} from 'redux'
import activeRepo from './activeRepoReducer'
import repos from './reposReducer'
import user from './userReducer'

export default combineReducers({
  activeRepo,
  repos,
  user
})

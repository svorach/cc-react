import {SET_ACTIVE_REPO} from 'constants/actionTypes'

const initialState = null

export default function activeRepo (state = initialState, action) {
  switch (action.type) {
    case SET_ACTIVE_REPO:
      return action.payload
    default:
      return state
  }
}

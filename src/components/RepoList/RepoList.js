import './repoList.scss'

import React from 'react'
import PropTypes from 'prop-types'

import Repo from '../Repo'

const RepoList = ({repos}) => {
  const mappedRepos = repos.map((repo, index) => {
    return <Repo key={`${repo.name}-${index}`} repo={repo} />
  })

  return (
    <div styleName='repoList'>
      <h2>Repos</h2>
      <ul>
        {mappedRepos}
      </ul>
    </div>
  )
}

RepoList.propTypes = {
  repos: PropTypes.array.isRequired
}

export default RepoList

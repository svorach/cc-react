import React from 'react'
import PropTypes from 'prop-types'

import {formatKeyForDisplay} from '../../utilities/formatters'

const RepoInfo = ({repo}) => {
  if (!repo) return null

  const mappedRepoInfo = Object.keys(repo).map((key, index) => {
    let repoProp = repo[key]

    return (repoProp && typeof repoProp !== 'object') ? <li key={`repo-${index}`}>
      <b>{formatKeyForDisplay(key)}:</b><span>{repoProp}</span>
    </li> : null
  })

  return (
    <ul>
      {mappedRepoInfo}
    </ul>
  )
}

RepoInfo.propTypes = {
  repo: PropTypes.object
}

export default RepoInfo

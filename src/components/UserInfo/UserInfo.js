import React from 'react'
import PropTypes from 'prop-types'

import {formatKeyForDisplay} from '../../utilities/formatters'

const UserInfo = ({user}) => {
  const userInfo = Object.keys(user).map((key, index) => {
    let userProp = user[key]

    // could write a generic filter to discard empty props
    return (userProp) ? <li key={`user-${index}`}>
      <b>{formatKeyForDisplay(key)}:</b><span>{userProp}</span>
    </li> : null
  })

  return (
    <div className='user-info'>
      <h2>{user.name}</h2>

      <img src={user.avatar_url} alt={user.login} width='200' height='200' />

      <ul>
        {userInfo}
      </ul>
    </div>
  )
}

UserInfo.propTypes = {
  user: PropTypes.object.isRequired
}

export default UserInfo

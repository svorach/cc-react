import React from 'react'
import PropTypes from 'prop-types'

import {NavLink} from 'react-router-dom'

const Repo = ({repo}) => {
  return (
    <li><NavLink to={`/repo/${repo.id}`}>{repo.name}</NavLink></li>
  )
}

Repo.propTypes = {
  repo: PropTypes.object.isRequired
}

export default Repo
